class QuestionListModel {
  String correctAns;
  String explaination;
  String mark;
  String options1;
  String options2;
  String options3;
  String options4;
  String qsn;
  String qsnNumber;

  QuestionListModel(
      {this.correctAns,
        this.explaination,
        this.mark,
        this.options1,
        this.options2,
        this.options3,
        this.options4,
        this.qsn,
        this.qsnNumber});

  QuestionListModel.fromJson(Map<String, dynamic> json) {
    correctAns = json['correctAns'];
    explaination = json['explaination'];
    mark = json['mark'];
    options1 = json['options1'];
    options2 = json['options2'];
    options3 = json['options3'];
    options4 = json['options4'];
    qsn = json['qsn'];
    qsnNumber = json['qsnNumber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['correctAns'] = this.correctAns;
    data['explaination'] = this.explaination;
    data['mark'] = this.mark;
    data['options1'] = this.options1;
    data['options2'] = this.options2;
    data['options3'] = this.options3;
    data['options4'] = this.options4;
    data['qsn'] = this.qsn;
    data['qsnNumber'] = this.qsnNumber;
    return data;
  }

  @override
  String toString() {
    return this.qsn;
  }

}