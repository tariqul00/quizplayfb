import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:quizplay/util/question_state_controller.dart';
import 'package:quizplay/widget/common_widget.dart';

radio(List<String> list, String radioButtonItem, String correctAns, {horizontal = true}) =>
    RadioButton(
      list,
      radioButtonItem,
      correctAns,
      horizontal,
    );

class RadioButton extends StatefulWidget {
  List<String> list;
  String radioButtonItem;
  String correctAns;
  bool isHorizontal;

  RadioButton(
    this.list,
    this.radioButtonItem,
    this.correctAns,
    this.isHorizontal, {
    Key key,
  }) : super(key: key);

  @override
  _RadioButtonState createState() =>
      _RadioButtonState(list, radioButtonItem, correctAns, isHorizontal);
}

class _RadioButtonState extends State<RadioButton> {
  List<String> _list;
  String radioButtonItem;
  String correctAns;
  bool isHorizontal;

  _RadioButtonState(this._list, this.radioButtonItem, this.correctAns, this.isHorizontal);

  @override
  Widget build(BuildContext context) {
    QuestionStateController _questionStateController = Get.find();
    return isHorizontal
        ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              for (var item in _list)
                InkWell(
                  onTap: () {
                    setState(() {
                    radioButtonItem = item;
                      _questionStateController.answerTab = item;
                      if (item == correctAns) {
                        snackBar(context, "Correct Answer");
                      }
                    });
                  },
                  child: Row(
                    children: [
                      SizedBox(
                        width: 8,
                      ),
                      SizedBox(
                        width: 20,
                        height: 20,
                        child: Radio(
                          activeColor: Colors.purple,
                          value: item,
                          groupValue: _questionStateController.answerTab,
                          onChanged: (val) {
                            setState(() {
                              radioButtonItem = item;
                              _questionStateController.answerTab = item;
                              if (item == correctAns) {
                                snackBar(context, "Correct Answer");
                              }
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      Text(item),
                      SizedBox(
                        width: 8,
                      ),
                    ],
                  ),
                )
            ],
          )
        : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              for (var item in _list)
                InkWell(
                  onTap: () {
                    setState(() {
                      radioButtonItem = item;
                      _questionStateController.answerTab = item;
                      if (item == correctAns) {
                       // snackBar(context, "Correct Answer");
                      }
                    });
                  },
                  child: Row(
                    children: [
                      SizedBox(
                        height: 8,
                      ),
                      SizedBox(
                        width: 24,
                        height: 24,
                        child: Radio(
                          activeColor: Colors.purple,
                          value: item,
                          groupValue: _questionStateController.answerTab,
                          onChanged: (val) {
                            setState(() {
                              radioButtonItem = item;
                              _questionStateController.answerTab = item;
                              if (item == correctAns) {
                               // snackBar(context, "Correct Answer");
                              }
                            });
                          },
                        ),
                      ),
                      SizedBox(
                        width: 4,
                      ),
                      //  Title14(item),
                      Text(item),
                      SizedBox(
                        height: 8,
                      ),
                    ],
                  ),
                )
            ],
          );
  }
}
