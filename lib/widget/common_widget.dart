import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quizplay/theme/light_theme.dart';
import 'package:quizplay/util/question_configuration_controller.dart';


snackBar(BuildContext context,String message) {
  return ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      content: Text(message),
      duration: Duration(seconds: 2),
    ),
  );
}

class DropDown extends StatefulWidget {
  List<String> list;
  bool isExpanded;
  bool isUnderline;

  DropDown(this.list, {this.isExpanded = false, this.isUnderline = true});

  @override
  State<StatefulWidget> createState() =>
      _DropDownState(list, isExpanded, isUnderline);
}

class _DropDownState extends State<DropDown> {
  List<String> _list;
  String _selecteditem;
  bool expanded;
  bool underline;

  _DropDownState(this._list, this.expanded, this.underline);

  @override
  Widget build(BuildContext context) {
    QuestionConfigurationController _questionController = Get.find();

    return DropdownButton(
      underline: underline == true ? null : SizedBox(),
      isExpanded: expanded,
      hint: Text(_list.first),
      value: _selecteditem,
      onChanged: (newValue) {
        setState(() {
          _selecteditem = newValue;
          _questionController.totalQuestion = newValue;
        });
      },
      items: _list.map((list) {
        return DropdownMenuItem(
          child: new Text(list),
          value: list,
        );
      }).toList(),
    );
  }
}

label(String title) => Text(title, style: TextStyle(fontSize: 16));
