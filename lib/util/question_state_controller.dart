import 'package:get/get.dart';
class QuestionStateController extends GetxController {
  String _answerTab = "";

  String get answerTab => _answerTab;

  set answerTab(String value) {
    _answerTab = value;
     update();
  }

}