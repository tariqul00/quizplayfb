import 'package:get/get.dart';
class QuestionConfigurationController extends GetxController {
  String _totalQuestion = "";

  String get totalQuestion => _totalQuestion;

  set totalQuestion(String totalQuestion) {
    _totalQuestion = totalQuestion;
     update();
  }

}