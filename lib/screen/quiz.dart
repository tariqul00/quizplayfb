import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:flutter/services.dart' show rootBundle;
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';
import 'package:quizplay/data_model/QuestionListModel.dart';
import 'package:quizplay/theme/light_theme.dart';
import 'package:quizplay/util/question_state_controller.dart';
import 'dart:async';

import 'package:quizplay/widget/RadioButton.dart';
import 'package:quizplay/widget/common_widget.dart';

class Quiz extends StatefulWidget {
  @override
  _QuizState createState() => _QuizState();
}

class _QuizState extends State<Quiz> {


  @override
  void initState() {
  //  _populateAnswerArray();
    _questionAnswerList();
    super.initState();
  }

 // List _questions = [];
  bool pressed = false;
  int _questionIndex = 0;
  int _totalScore = 0;
  List _currScore;
  List _currAns;
  String resetValue = "";
  List<QuestionListModel>  _questionAnswer;

  Future<String> _loadAQuestionAsset() async {
    return await rootBundle.loadString('assets/questions2.json');
  }

  Future<List<QuestionListModel>> _questionAnswerList() async {
    String jsonString = await _loadAQuestionAsset();
    Iterable l = json.decode(jsonString);
    List<QuestionListModel> garageModel = List<QuestionListModel>.from(l.map((model)=> QuestionListModel.fromJson(model)));
    setState(() {
      _questionAnswer = garageModel;
      print("_questionAnswer " + _questionAnswer.length.toString());
    });
  }




  @override
  Widget build(BuildContext context) {
    QuestionStateController _questionStateController = Get.find();
    var options = [_questionAnswer[_questionIndex].options1,_questionAnswer[_questionIndex].options2, _questionAnswer[_questionIndex].options3,_questionAnswer[_questionIndex].options4];
    //_questionAnswerList().toString();
   // print(_questionAnswerList().toString());
    return Scaffold(
      appBar: AppBar(
        title: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(Icons.account_box, size: 22),
              ),
              TextSpan(
                  text: "Quiz Play",
                  style:
                  TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        centerTitle: true,
        backgroundColor: light.primaryColor,
      ),
      body: SingleChildScrollView(
        padding: EdgeInsets.all(20.0),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 16),
            Text(_questionAnswer[_questionIndex].qsn),
            SizedBox(height: 16),
            radio(options,resetValue, options[int.parse(_questionAnswer[_questionIndex].correctAns) - 1 ], horizontal: false),
            SizedBox(height: 32),

            GetBuilder<QuestionStateController>(
              init: _questionStateController,
              builder: (controller) {
                return Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    ElevatedButton(onPressed: () {
                      if (_questionIndex == 0) {
                        return;
                      }
                      controller.answerTab = "";
                      setState(() {
                        _questionIndex = _questionIndex - 1;
                        resetValue = "";
                      });
                    }, child: Text(" Previous ")),
                    ElevatedButton(onPressed: () {
                      if (_questionIndex == _questionAnswer.length - 1) {
                        return;
                      }
                      controller.answerTab = "";
                      setState(() {
                        _questionIndex = _questionIndex + 1;
                        resetValue = "";
                      });
                    }, child: Text(" Next "))
                  ],
                );
              },
            ),

          ],
        ),
      ),
    );

  }
}
