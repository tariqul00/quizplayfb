import 'package:flutter/material.dart';
import 'package:launch_review/launch_review.dart';
import 'package:quizplay/screen/controller.dart';
import 'package:quizplay/screen/quiz.dart';
import 'package:quizplay/screen/scoreStorage.dart';
import 'package:quizplay/screen/aboutus.dart';
import 'package:quizplay/screen/trip_exam_formet.dart';
import 'package:quizplay/theme/dark_theme.dart';
import 'package:quizplay/theme/light_theme.dart';
import 'package:quizplay/util/app_constants.dart';
import 'package:quizplay/widget/common_widget.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {

    final myImageAndCaption = [
      ["assets/images/practice.png", "Practice"],
      ["assets/images/mockup.png", "Quick Mockup"],
      ["assets/images/mockup.png", "Pop Mock"],
      ["assets/images/previousrecord.png", "Previous Record"],
      ["assets/images/tripexamformat.png", "Trip & Exam Format"],
      ["assets/images/about_us.png", "About Us"],
      ["assets/images/rating.png", "Rate Us"],
      ["assets/images/share.png", "Share App"],
    ];

    return Scaffold(
      appBar: AppBar(
        title: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(Icons.account_box, size: 22),
              ),
              TextSpan(
                  text: AppConstants.AppName,
                  style:
                  TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        centerTitle: true,
        backgroundColor: light.primaryColor,
      ),
      body: GridView.count(
        crossAxisCount: 2,
        children: [
          ...myImageAndCaption.map(
                (i) => Card(

              child: new InkWell(
                onTap: () {
                  print("InkResponse " + i.last);
                  //Navigator.pushNamed(context, MyRoutes.vehicleInfo);

                  if(i.last == "Practice"){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Controller(requestType: "Practice")));
                  }else if(i.last == "Quick Mockup"){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Quiz()));
                  }else if(i.last == "Pop Mock"){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => Quiz()));
                  }else if(i.last == "Previous Record"){

                  } else if(i.last == "Trip & Exam Format"){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => TripExamFormat()));
                  }else if(i.last == "About Us"){
                    Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => AboutUs()));
                  }else if(i.last == "Rate Us"){
                    LaunchReview.launch(androidAppId: "com.iyaffle.rangoli",
                        iOSAppId: "585027354");
                  }
                  else if(i.last == "Share App"){
                    //Share.share('check out my website https://example.com');
                    Share.share(
                        "message",
                        sharePositionOrigin: Rect.fromLTWH(0, 0, 200, 200 / 2));
                  }
                },
                child: Center(
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Material(
                        child: Image.asset(
                          i.first,
                          fit: BoxFit.cover,
                          height: 64,
                          width: 64,
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text(i.last, style: TextStyle(fontSize: 16, fontWeight: FontWeight.w500),),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );

  }
}
