import 'dart:async';
import 'package:flutter/material.dart';
import 'package:quizplay/screen/home.dart';
import 'package:quizplay/screen/quiz.dart';
import 'package:quizplay/theme/light_theme.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(Duration(milliseconds: 1500), () {
      Navigator.of(context).pushReplacement(MaterialPageRoute(
        builder: (context) => HomePage(),
      ));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: light.primaryColor,
      body: Center(
          child: Text(
        'QuizPlay',
        style: TextStyle(
            fontSize: 30.0, fontWeight: FontWeight.bold, color: Colors.white),
      )),
    );
  }
}
