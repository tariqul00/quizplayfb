import 'package:flutter/material.dart';
import 'package:quizplay/theme/light_theme.dart';

class TripExamFormat extends StatelessWidget {
  const TripExamFormat({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(Icons.account_box, size: 22),
              ),
              TextSpan(
                  text: "About Us",
                  style:
                  TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        centerTitle: true,
        backgroundColor: light.primaryColor,
      ),
      body: Center(
        child: Column(children: [
          Card(
            child: Text(
              'About fishingstarterkit.com '
                  'The fishing starter kit was created to provide advanced material, guiding you towards a better and more comfortable fishing experience. It makes you easy to have your own aquarium.'
                  'Our Mission'
                  'Fishkeeping or Fishing is one of the most fun hobbies in the world. We are buying a fishing tank or materials that we need to catch fish. Most of the time we buy the wrong one.'
                  'fishingstarterkit.com mission is to help through review, ratings, and our detailed suggestions. We hope you will get the correct direction and '
                  'you can read through our step by step guide.'
                  'Need Your Suggestions'
                  'if you have any queries or need my suggestions.',
            ),
          ),
        ]),
      ),
    );
  }
}
