import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quizplay/screen/quiz.dart';
import 'package:quizplay/theme/light_theme.dart';
import 'package:quizplay/util/question_configuration_controller.dart';
import 'package:quizplay/widget/common_widget.dart';

class Controller extends StatelessWidget {
  String requestType;

  Controller({Key key, @required this.requestType}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    QuestionConfigurationController _questionController = Get.find();

    return Scaffold(
      appBar: AppBar(
        title: RichText(
          text: TextSpan(
            children: [
              WidgetSpan(
                child: Icon(Icons.account_box, size: 22),
              ),
              TextSpan(
                  text: requestType,
                  style:
                      TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold)),
            ],
          ),
        ),
        centerTitle: true,
        backgroundColor: light.primaryColor,
      ),
      body: GetBuilder<QuestionConfigurationController>(
          init: _questionController,
          builder: (controller) {
            return Card(
              margin: EdgeInsets.all(16),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [label("Mark"), label((int.parse(controller.totalQuestion) * 2).toString())]),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [label("Time"), label(controller.totalQuestion * 1 + " min")]),
                      SizedBox(
                        height: 8,
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [label("Total Question"), label(controller.totalQuestion)]),
                      SizedBox(
                        height: 32,
                      ),
                      label("Choose number of questions:"),
                      SizedBox(
                        height: 8,
                      ),
                      Row(children: [
                        label("Set A, 2 Mark Question "),
                        SizedBox(width: 10),
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.symmetric(horizontal: 16.0),
                            child: DropDown(
                              ["0", "1", "2", "3", "4", "5"],
                              isExpanded: true,
                            ),
                          ),
                        ),
                      ]),
                      Center(
                        child: ElevatedButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Quiz()));
                            },
                            child: Text(" Start ")),
                      ),
                    ]),
              ),
            );
          }),
    );
  }
}
