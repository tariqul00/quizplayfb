import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';
import 'package:quizplay/screen/splash.dart';
import 'package:quizplay/theme/dark_theme.dart';
import 'package:quizplay/theme/light_theme.dart';
import 'package:quizplay/util/question_configuration_controller.dart';
import 'package:quizplay/util/question_state_controller.dart';

void main() {

  Get.lazyPut(() => QuestionStateController());
  Get.lazyPut(() => QuestionConfigurationController());

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      title: 'Quiz Play',
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.system,
      theme: light,
      darkTheme: dark,
      home: SplashScreen(),
    );
  }
}